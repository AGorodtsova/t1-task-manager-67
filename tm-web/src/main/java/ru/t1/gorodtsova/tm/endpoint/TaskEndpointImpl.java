package ru.t1.gorodtsova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RestController
@RequestMapping("api/tasks")
public class TaskEndpointImpl {

    @Autowired
    private ITaskService taskService;

    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return taskService.findById(id);
    }

    @WebMethod
    @PostMapping("/save")
    public void save(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        taskService.save(task);
    }

    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        taskService.removeById(id);
    }

    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    ) {
        taskService.remove(task);
    }

}
