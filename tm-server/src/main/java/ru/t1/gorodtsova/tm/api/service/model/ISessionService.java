package ru.t1.gorodtsova.tm.api.service.model;

import ru.t1.gorodtsova.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
