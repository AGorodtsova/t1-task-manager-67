-- Table: public.tm_session

-- DROP TABLE IF EXISTS public.tm_session;

CREATE TABLE IF NOT EXISTS public.tm_session
(
    id character varying(100) COLLATE pg_catalog."default" NOT NULL,
    created time without time zone NOT NULL,
    user_id character varying(100) COLLATE pg_catalog."default",
    role character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT tm_session_pk PRIMARY KEY (id),
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.tm_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_session
    OWNER to postgres;

COMMENT ON TABLE public.tm_session
    IS 'TASK MANAGER SESSION TABLE';