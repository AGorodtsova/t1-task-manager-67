package ru.t1.gorodtsova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.project.ProjectStartByIdRequest;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIdListener extends AbstractProjectListener {

    @NotNull
    private final String DESCRIPTION = "Start project by id";

    @NotNull
    private final String NAME = "project-start-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectStartByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(getToken(), id);
        projectEndpoint.startProjectById(request);
    }

}
